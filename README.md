# Cosmopolis

## Présentation

Au début de ma deuxième année, nous avions du réaliser un jeu avec la méthode agile SCRUM, un projet que nous avions dû réaliser en trois jours.

Le jeu que j'ai réalisé avec Aymeri [Tourneur](mailto:aymeri.tourneur.etu@univ-lille.fr), Baptiste [Lebon](mailto:baptiste.lebon.etu@univ-lille.fr), Mathis [Decoster](mailto:mathis.decoster.etu@univ-lille.fr) et Karim [Aoulad-Tayab](mailto:karim.aoulad-tayab.etu@univ-lille.fr) est un clicker de simulation de ville où nous devons acheter des bâtiments afin d'envoyer une fusée dans l'espace.
